package super_go_package

import (
	"github.com/fatih/color"
	"strconv"
)

// fibonacci is a function that returns
// a function that returns an int.
//v0.2.0 icin onemli ekleme yaptim
func fibonacci() func() int {
	x, y := 0, 1
	return func() int {
		x, y = y, x + y
		return x
	}
}

func Fibonacci(count int) {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		color.Blue(strconv.Itoa(f()))
	}
}